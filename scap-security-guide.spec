%undefine __cmake_in_source_build
%global _vpath_builddir build

Name:		scap-security-guide
Version:	0.1.68
Release:	8
Summary:	Security guidance and baselines in SCAP formats
License:	BSD-3-Clause
URL:		https://github.com/ComplianceAsCode/content/
Source0:	https://github.com/ComplianceAsCode/content/releases/download/v%{version}/scap-security-guide-%{version}.tar.bz2

Patch0001: add-openeuler-support.patch
Patch0002: add-openeuler-control-rules.patch
Patch0003: optimize-rules-for-openEuler.patch
Patch0004: add-openeuler-automatic-hardening.patch
Patch0005: scap-is-modified-to-be-consistent-with-the-specif.patch
Patch0006: scap-security-guide-0.1.68-port-to-newer-cmake.patch

BuildArch:	noarch
BuildRequires: 	libxslt, expat, python3, openscap-scanner >= 1.2.5, cmake >= 3.12, python3-jinja2, python3-PyYAML
Requires:      	xml-common, openscap-scanner >= 1.2.5
Obsoletes:	openscap-content < 0:0.9.13
Provides:	openscap-content
%description
The scap-security-guide project provides a guide for configuration of the
system from the final system's security point of view. The guidance is specified
in the Security Content Automation Protocol (SCAP) format and constitutes
a catalog of practical hardening advice, linked to government requirements
where applicable. The project bridges the gap between generalized policy
requirements and specific implementation guidelines. The Fedora system
administrator can use the oscap CLI tool from openscap-scanner package, or the
scap-workbench GUI tool from scap-workbench package to verify that the system
conforms to provided guideline. Refer to scap-security-guide(8) manual page for
further information.

%package	doc
Summary:	HTML formatted security guides generated from XCCDF benchmarks
Requires:       %{name} = %{version}-%{release}
%description	doc
The %{name}-doc package contains HTML formatted documents containing
hardening guidances that have been generated from XCCDF benchmarks
present in %{name} package.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%cmake -DSSG_SCE_ENABLED=ON
%cmake_build

%install
%cmake_install

rm -f %{buildroot}%{_docdir}/%{name}/LICENSE

%files
%license LICENSE
%{_datadir}/xml/scap/ssg/content
%{_datadir}/%{name}/kickstart
%{_datadir}/%{name}/ansible
%{_datadir}/%{name}/tailoring
%{_datadir}/%{name}/bash
%{_mandir}/man8/scap-security-guide.8.*
%doc %{_docdir}/%{name}/README.md
%doc %{_docdir}/%{name}/Contributors.md

%files doc
%doc %{_docdir}/%{name}/guides/*.html
%doc %{_docdir}/%{name}/tables/*.html

%changelog
* Fri Mar 07 2025 Funda Wang <fundawang@yeah.net> - 0.1.68-8
- fix build with cmake 4.0

* Wed Dec 18 2024 zcfsite <zhchf2010@126.com> - 0.1.68-7
- add automatic hardening and fix consistent with the baseline

* Wed Dec 4 2024 steven <steven_ygui@163.com> - 0.1.68-6 
- automatic hardening is supported.

* Mon Nov 11 2024 Funda Wang <fundawang@yeah.net> - 0.1.68-5
- adopt to new cmake macro

* Sat Feb 24 2024 wangqingsan <wangqingsan@huawei.com> - 0.1.68-4
- optimiz rules for openEuler

* Mon Feb 19 2024 steven <steven_ygui@163.com> - 0.1.68-3
- add openEuler 2403 LTS supporting and remove openEuler general version supporting

* Mon Oct 9 2023 steven <steven_ygui@163.com> - 0.1.68-2
- add openeuler supporting and add 100+ control rules

* Tue Jul 18 2023 xu_ping <707078654@qq.com> - 0.1.68-1
- Upgrade to 0.1.68

* Fri Dec 30 2022 xuxinyu <xuxinyu@xfusion.com> - 0.1.49-3
- Fix test scenarios for OSPP profile

* Sat Feb 26 2022 HuaxinLu <luhuaxin1@huawei.com> - 0.1.49-2
- fix for python upgrade

* Wed Oct 28 2020 Anan Fu <fuanan3@huawei.com> - 0.1.49-1
- upgrade to disable python2

* Mon Oct 14 2019 dongjian <dongjian13@huawei.com> - 0.1.39-4
- Package init
